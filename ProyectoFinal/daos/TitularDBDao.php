<?php namespace daos;

use daos\Conexion as Conexion;
use daos\UsuarioDBDao as UsuarioDao;
use modelos\Titular as Titular;
use modelos\Usuario as Usuario;

class TitularDBDao extends Conexion /*implements InterfaceDao*/{

    protected $tabla = "Titulares";
    private $daoUsuario;
    private static $instancia;


    function __construct(){

      $this->daoUsuario = UsuarioDao::getInstance();
    }

    public static function getInstance(){
    if (  !self::$instancia instanceof self)
    {
       self::$instancia = new self;
    }
    return self::$instancia;
  }

    public function buscarPorDni($dni) {

      $sql = "SELECT nombre,apellido,dni,telefono,id_usuario FROM " . $this->tabla . " WHERE dni = :dni";

      $obj_pdo = Conexion::getInstance();

      $conexion = $obj_pdo->conectar();
      $sentencia=$conexion->prepare($sql);
      $sentencia->bindParam(":dni",$dni);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {
        $usuario = $this->daoUsuario->traerUno($row['id_usuario']);
        $titular = new Titular($row['apellido'],$row['nombre'],$row['dni'],$row['telefono'],$usuario);
        return $titular;
      }
    }

    public function buscarPorId($id_titular) {

      $sql = "SELECT nombre,apellido,dni,telefono,id_usuario FROM " . $this->tabla . " WHERE id_titular = :id_titular";

      $obj_pdo = Conexion::getInstance();

      $conexion = $obj_pdo->conectar();
      $sentencia=$conexion->prepare($sql);
      $sentencia->bindParam(":id_titular",$id_titular);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {
        $usuario = $this->daoUsuario->traerUno($row['id_usuario']);
        $titular = new Titular($row['apellido'],$row['nombre'],$row['dni'],$row['telefono'],$usuario);
        return $titular;
      }
    }

    public function traerTodos() {

        // Guardo como string la consulta sql
        $sql = "SELECT * FROM " . $this->tabla;


        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();

        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();

        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);

        // Ejecuto la sentencia.
        $sentencia->execute();

        while ($row = $sentencia->fetch()) {

          $usuario = $this->daoUsuario->traerUno($row['id_usuario']);


          //Para cada row que trae, crea un titular seteandole los datos de cada columna
          //lo guarda en un arreglo de objetos titular
          $titular = new Titular($row['apellido'],$row['nombre'],$row['dni'],$row['telefono'],$usuario);
				$titulares[] = $titular;

			}


      //Si el arreglo no esta vacio lo devuelve
      			if(!empty($titulares)) return $titulares;

    }



    public function agregar(Titular $titular) {
        // Guardo como string la consulta sql utilizando como values, marcadores de parámetros con nombre (:name) o signos de interrogación (?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada
        $sql = "INSERT INTO " . $this->tabla . " (nombre,apellido,dni,telefono,id_usuario) VALUES (:nombre,:apellido,:dni,:telefono,:id_usuario)";

        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();
        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();
        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);

        $nombre=$titular->getNombre();
        $apellido=$titular->getApellido();
        $dni=$titular->getDni();
        $telefono=$titular->getTelefono();
        $usuario=$titular->getUsuario();
        $id_usuario = $this->daoUsuario->agregar($usuario);
        //Guardamos el usuario creado en la base de datos

        $sentencia->bindParam(":nombre", $nombre);
        $sentencia->bindParam(":apellido", $apellido);
        $sentencia->bindParam(":dni", $dni);
        $sentencia->bindParam(":telefono", $telefono);
        $sentencia->bindParam(":id_usuario", $id_usuario);

        // Ejecuto la sentencia.
       $sentencia->execute();

    }

    public function buscarId(Titular $titular){

      $sql = "SELECT id_titular FROM " . $this->tabla . " WHERE dni = :dni";

      $obj_pdo = Conexion::getInstance();

      $conexion = $obj_pdo->conectar();
      $sentencia=$conexion->prepare($sql);

      $dni=$titular->getDni();
      $sentencia->bindParam(":dni",$dni);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {
        return $row['id_titular'];
      }

    }
}

?>
