<?php namespace daos;

use daos\Conexion as Conexion;
use modelos\Sensor_Semaforo as SensorSemaforo;
use modelos\Sensor as Sensor;
use daos\SensorDBDao as SensorDao;


class SensorSemaforoDBDao extends Conexion  {

    protected $tabla = "SensorSemaforo";
    private static $instancia;
    private $daoSensor;


    function __construct(){

      $this->daoSensor = SensorDao::getInstance();
    }

    public static function getInstance(){
          if (  !self::$instancia instanceof self)
          {
             self::$instancia = new self;
          }
          return self::$instancia;
        }

    public function traerTodos() {

        // Guardo como string la consulta sql
        $sql = "SELECT * FROM " . $this->tabla;


        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();

        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();

        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);

        // Ejecuto la sentencia.
        $sentencia->execute();

        while ($row = $sentencia->fetch()) {

          $sensor=$this->daoSensor->buscarPorId($row['id_sensor']);
          //Para cada row que trae, crea un titular seteandole los datos de cada columna
          //lo guarda en un arreglo de objetos titular
          $sensorSemaforo = new SensorSemaforo($sensor->getFechaAlta(), $sensor->getLatitud(), $sensor->getLongitud(), $sensor->getNumeroSerie());

				$sensores[] = $sensorSemaforo;


			}
      //Si el arreglo no esta vacio lo devuelve
      			if(!empty($sensores)) return $sensores;

    }

    public function obtenerId(SensorSemaforo $sensorSemaforo){

        $sql = "SELECT id_sensorSemaforo FROM " . $this->tabla. " WHERE :id_sensor = id_sensor";

        $obj_pdo = Conexion::getInstance();

        $conexion = $obj_pdo->conectar();
        $sentencia=$conexion->prepare($sql);


        $id_sensor=$this->daoSensor->obtenerId($sensorSemaforo->getNumeroSerie());
        $sentencia->bindParam(":id_sensor",$id_sensor);
        var_dump($id_sensor);
        $sentencia->execute();
        $row = $sentencia->fetch();
        if($row) {

          return $row['id_sensorSemaforo'];
          var_dump($row['id_sensorSemaforo']);
        }


      }

      public function buscarPorId($id_sensorSemaforo) {

        $sql = "SELECT * FROM " . $this->tabla . " WHERE id_sensorSemaforo= :id_sensorSemaforo";
        $obj_pdo = Conexion::getInstance();
        $conexion = $obj_pdo -> conectar();
        $sentencia= $conexion->prepare($sql);
        $sentencia->bindParam(":id_sensorSemaforo",$id_sensorSemaforo);

        $sentencia->execute();
        $row = $sentencia->fetch();
        if($row) {

          $sensor=$this->daoSensor->buscarPorId($row['id_sensor']);
        $sensorSemaforo = new SensorSemaforo($sensor->getFechaAlta(), $sensor->getLatitud(), $sensor->getLongitud(), $sensor->getNumeroSerie());
          return $sensorSemaforo;

        }
      }


}

?>
