<?php namespace daos;

use daos\Conexion as Conexion;
use modelos\Rol as Rol;

class RolDBDao extends Conexion /*implements InterfaceDao*/{

    protected $tabla = "Roles";
    private static $instancia;


    public static function getInstance()
    {
    if (  !self::$instancia instanceof self)
    {
       self::$instancia = new self;
    }
    return self::$instancia;
  }
    public function traerUno($id_rol) {
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id_rol= :id_rol ";
      $obj_pdo = Conexion::getInstance();
      $conexion = $obj_pdo -> conectar();
      $sentencia= $conexion->prepare($sql);

      $sentencia->bindParam(":id_rol",$id_rol);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {

        $rol = new Rol($row['descripcion']);
        return $rol;

      }

    }



  }
