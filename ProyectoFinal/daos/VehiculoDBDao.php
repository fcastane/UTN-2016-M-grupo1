<?php namespace daos;

use daos\Conexion as Conexion;
use daos\TitularDBDao as TitularDao;
use daos\CuentaCteDBDao as CuentaCteDao;

use modelos\CuentaCte as CuentaCte;
use modelos\Titular as Titular;
use modelos\Vehiculo as Vehiculo;



class VehiculoDBDao extends Conexion {
    protected $tabla = "Vehiculos";
    private $daoTitular;
    private $daoCuentaCte;
    private static $instancia;


    function __construct(){

      $this->daoTitular = TitularDao::getInstance();
      $this->daoCuentaCte = CuentaCteDao::getInstance();
    }

    public static function getInstance(){
        if (  !self::$instancia instanceof self) {
       self::$instancia = new self;
      }
     return self::$instancia;
        }

    public function buscarPorDominio($dominio) {

      $sql = "SELECT dominio,marca,modelo,qr,id_titular,id_cuentaCte FROM " . $this->tabla . " WHERE dominio = :dominio";
      $obj_pdo = Conexion::getInstance();
      $conexion = $obj_pdo -> conectar();
      $sentencia= $conexion->prepare($sql);
      $sentencia->bindParam(":dominio",$dominio);
      $sentencia->execute();

      $row = $sentencia->fetch();
      if($row) {
          //Traigo un titular
          $titular = $this->daoTitular->buscarPorId($row['id_titular']);
          //Traigo cuentaCte
          $cuentaCte=$this->daoCuentaCte->buscarPorId($row['id_cuentaCte']);
          //Para cada row que trae, crea un vehiculo seteandole los datos de cada columna
          $vehiculo = new Vehiculo($row['dominio'],$row['marca'],$row['modelo'],$row['qr'],$titular,$cuentaCte);
          //Guardo este vehiculo en el arreglo
           return $vehiculo;
      }


    }

    public function traerTodos() {

        // Guardo como string la consulta sql
        $sql = "SELECT * FROM " . $this->tabla;


        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();

        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();

        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);

        // Ejecuto la sentencia.
        $sentencia->execute();

        while ($row = $sentencia->fetch()) {
          //Traigo un titular
          $titular = $this->daoTitular->buscarPorId($row['id_titular']);
          //Traigo cuentaCte
       $cuentaCte=$this->daoCuentaCte->buscarPorId($row['id_cuentaCte']);
          //Para cada row que trae, crea un vehiculo seteandole los datos de cada columna
          $vehiculo = new Vehiculo($row['dominio'],$row['marca'],$row['modelo'],$row['qr'],$titular,$cuentaCte);
          //Guardo este vehiculo en el arreglo
				$vehiculos[] = $vehiculo;


			}


      //Si el arreglo no esta vacio lo devuelve
      			if(!empty($vehiculos)) return $vehiculos;

    }

    public function agregar( $vehiculo) {

        // Guardo como string la consulta sql utilizando como values, marcadores de parámetros con nombre (:name) o signos de interrogación (?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada
        $sql = "INSERT INTO " . $this->tabla . " (dominio,marca,modelo,qr,id_titular,id_cuentaCte) VALUES (:dominio,:marca,:modelo,:qr,:id_titular,:id_cuentaCte)";

        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();
        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();
        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);


        $dominio=$vehiculo->getDominio();
        $marca=$vehiculo->getMarca();
        $modelo=$vehiculo->getModelo();
        $qr=$vehiculo->getQr();
        $titular=$vehiculo->getTitular();
        $cuentaCte=$vehiculo->getCuentaCte();

        $id_cuentaCte=$this->daoCuentaCte->agregar($cuentaCte); //Cuando agrega una cuentaCte me devuelve el last_insert_id();
        $id_titular= $this->daoTitular->buscarId($titular); // buscarId devuelve el Id de un titular;

        // Reemplazo los marcadores de parametro por los valores reales utilizando el método bindParam().
        $sentencia->bindParam(":dominio", $dominio);
        $sentencia->bindParam(":marca",$marca);
        $sentencia->bindParam(":modelo",$modelo);
        $sentencia->bindParam(":qr",$qr);
        $sentencia->bindParam(":id_titular",$id_titular);
        $sentencia->bindParam(":id_cuentaCte",$id_cuentaCte);

        // Ejecuto la sentencia.
        $sentencia->execute();

    }

    public function traerPorIdTitular($id_titular) {

        // Guardo como string la consulta sql
        $sql = "SELECT * FROM " . $this->tabla . " WHERE id_titular = :id_titular";


        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();

        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();

        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);

        $sentencia->bindParam(":id_titular",$id_titular);
        // Ejecuto la sentencia.
        $sentencia->execute();

        while ($row = $sentencia->fetch()) {

          $titular = $this->daoTitular->buscarPorId($row['id_titular']);
          $cuentaCte=$this->daoCuentaCte->buscarPorId($row['id_cuentaCte']);
          //Para cada row que trae, crea un vehiculo seteandole los datos de cada columna
          $vehiculo = new Vehiculo($row['dominio'],$row['marca'],$row['modelo'],$row['qr'],$titular,$cuentaCte);
          //Guardo este vehiculo en el arreglo
        $vehiculos[] = $vehiculo;


      }


      //Si el arreglo no esta vacio lo devuelve
            if(!empty($vehiculos)) return $vehiculos;

    }



}

?>
