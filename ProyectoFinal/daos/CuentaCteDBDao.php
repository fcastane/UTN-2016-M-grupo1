<?php namespace daos;

use daos\Conexion as Conexion;
use modelos\CuentaCte as CuentaCte;


class CuentaCteDBDao extends Conexion /*implements InterfaceDao*/{

    protected $tabla = "CuentasCte";
    private static $instancia;


    function __construct(){

    }

    public static function getInstance(){
        if (  !self::$instancia instanceof self)
        {
           self::$instancia = new self;
        }
        return self::$instancia;
      }

    public function buscarPorId($id_cuentaCte) { //retorna una CuentaCorriente segun el id ingresado

      $sql = "SELECT * FROM " . $this->tabla . " WHERE id_cuentaCte = :id_cuentaCte";
      $obj_pdo = Conexion::getInstance();
      $conexion = $obj_pdo -> conectar();
      $sentencia= $conexion->prepare($sql);
      $sentencia->bindParam(":id_cuentaCte",$id_cuentaCte);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {

        $cuentaCte = new cuentaCte($row['fecha_ultActualizacion'],$row['saldo'],$row['id_cuentaCte']);
        return $cuentaCte;
      }
    }

    public function traerTodos() { //retorna un array con todas las cuentas corrientes

        // Guardo como string la consulta sql
        $sql = "SELECT * FROM " . $this->tabla;


        $obj_pdo = Conexion::getInstance();
        $conexion = $obj_pdo->conectar();
        $sentencia = $conexion->prepare($sql);

        $sentencia->execute();

        while ($row = $sentencia->fetch()) {

          $cuentaCte = new cuentaCte($row['fecha_ultActualizacion'],$row['saldo'],$row['id_cuentaCte']);

				$cuentasCte[] = $cuentaCte;

			}
      //Si el arreglo no esta vacio lo devuelve
      			if(!empty($cuentasCte)) return $cuentasCte;

    }

    public function agregar(CuentaCte $cuentaCte) { //agregar una cuentaCte a la bd y devuelve el id de la cuenta que se ingreso

        $sql = "INSERT INTO " . $this->tabla . " (fecha_ultActualizacion,saldo) VALUES (NOW(),:saldo)";

        $obj_pdo = Conexion::getInstance();
        $conexion = $obj_pdo->conectar();
        $sentencia = $conexion->prepare($sql);

        $saldo=$cuentaCte->getSaldo();

        $sentencia->bindParam(":saldo", $saldo);
        $sentencia->execute();

        $lastId = $conexion->lastInsertId();

         return $lastId;

    }

    public function actualizar($numero,$importe)  { //actualiza para un numero de cuenta, el saldo para now()

            // Guardo como string la consulta sql utilizando como values, marcadores de parámetros con nombre (:name) o signos de interrogación (?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada
            $sql = "UPDATE ".$this->tabla. " SET fecha_ultActualizacion=now(),saldo=saldo-:saldo WHERE id_cuentaCte=:id_cuentaCte";

            // creo el objeto conexion
            $obj_pdo = Conexion::getInstance();
            // Conecto a la base de datos.
            $conexion = $obj_pdo->conectar();
            // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
            $sentencia = $conexion->prepare($sql);

            $sentencia->bindParam(":id_cuentaCte", $numero);
            $sentencia->bindParam(":saldo", $importe);

            // Ejecuto la sentencia.
            $sentencia->execute();


        }



}

?>
