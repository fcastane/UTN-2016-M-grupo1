<?php namespace daos;

use daos\Conexion as Conexion;
use daos\SensorSemaforoDBDao as SensorSemaforoDBDao;
use modelos\Sensor as Sensor;
use daos\IDaoSensor as IDaoSensor;


class SensorDBDao extends Conexion {

    protected $tabla = "Sensores";
    private static $instancia;


    function __construct(){

    }

    public static function getInstance(){
        if (  !self::$instancia instanceof self)
        {
           self::$instancia = new self;
        }
        return self::$instancia;
      }

    public function buscarPorId($id_sensor) {

      $sql = "SELECT * FROM " . $this->tabla . " WHERE id_sensor = :id_sensor";
      $obj_pdo = Conexion::getInstance();
      $conexion = $obj_pdo -> conectar();
      $sentencia= $conexion->prepare($sql);
      $sentencia->bindParam(":id_sensor",$id_sensor);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {

        $sensor = new Sensor($row['fecha_alta'], $row['latitud'], $row['longitud'], $row['numero_serie']);
        return $sensor;

      }
    }

    public function buscarPorNumeroSerie($numeroSerie) {

      $sql = "SELECT * FROM " . $this->tabla . " WHERE numero_serie = :numero_serie";
      $obj_pdo = Conexion::getInstance();
      $conexion = $obj_pdo -> conectar();
      $sentencia= $conexion->prepare($sql);
      $sentencia->bindParam(":numero_serie",$numeroSerie);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {

        $sensor = new Sensor($row['fecha_alta'], $row['latitud'], $row['longitud'], $row['numero_serie']);
        return $sensor;

      }
    }

    public function obtenerId($numeroSerie) {

      $sql = "SELECT id_sensor FROM " . $this->tabla . " WHERE numero_serie = :numero_serie";
      $obj_pdo = Conexion::getInstance();
      $conexion = $obj_pdo -> conectar();
      $sentencia= $conexion->prepare($sql);
      $sentencia->bindParam(":numero_serie",$numeroSerie);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {

        return $row['id_sensor'];

      }
    }

  }

?>
