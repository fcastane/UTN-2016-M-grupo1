<?php namespace daos;


use daos\Conexion as Conexion;
use daos\EventoMultaDBDao as EventoMultaDao;
use daos\EventoPeajeDBDao as EventoPeajeDao;

use daos\TarifaDBDao as TarifaDao;
use daos\CuentaCteDBDao as CuentaCteDao;
use modelos\CuentaCte as CuentaCte;
use modelos\MovimientoCtaCte as MovimientoCtaCte;

class MovimientoCtaCteDBDao extends Conexion /*implements InterfaceDao*/{

    protected $tabla = "MovimientosCtaCte";
    private static $instancia;
    private $daoEventoMulta;
    private $daoEventoPeaje;
    private $daoTarifa;
    private $daoCuentaCte;

    function __construct(){
      $this->daoEventoMulta= EventoMultaDao::getInstance();
      $this->daoEventoPeaje= EventoPeajeDao::getInstance();
      $this->daoTarifa=TarifaDao::getInstance();
      $this->daoCuentaCte = CuentaCteDao::getInstance();
    }

  public static function getInstance(){
      if (  !self::$instancia instanceof self)
      {
         self::$instancia = new self;
      }
      return self::$instancia;
    }

  public function agregar(MovimientoCtaCte $movimiento) {

        // Guardo como string la consulta sql utilizando como values, marcadores de parámetros con nombre (:name) o signos de interrogación (?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada
        $sql = "INSERT INTO " . $this->tabla . " (fecha_hora,importe,id_cuentaCte,id_eventoMulta,id_eventoPeaje) VALUES (NOW(),:importe,:id_cuentaCte,:id_eventoMulta,:id_eventoPeaje)";

        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();
        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();
        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);

        $cuentaCte = $movimiento->getCuentaCte();

        $id_cuentaCte=$cuentaCte->getNumero();

      if($movimiento->getEventoMulta()){ //Si el tipo de evento es multa(este no es null)

          $id_evento = $this->daoEventoMulta->agregar($movimiento->getEventoMulta());//agrego el evento al bd
          $tarifa=$this->daoTarifa->traerUno('1');
          $importe=$tarifa->getMulta();//traigo el importe de una multa
          $sentencia->bindParam(":id_eventoMulta", $id_evento);
          $sentencia->bindValue(":id_eventoPeaje", null);


     }

       else if($movimiento->getEventoPeaje()){ //Si el tipo de evento es peaje

         $id_evento = $this->daoEventoPeaje->agregar($movimiento->getEventoPeaje());
         $tarifa=$this->daoTarifa->traerUno('1');
         $franjaHoraria=$movimiento->getEventoPeaje()->getFranja();


         if($franjaHoraria=='Pico'){ //Si la franja horrio pico, traigo una tarifa

         $importe=$tarifa->getPico();


       }
       else $importe=$tarifa->getNoPico();


         $sentencia->bindParam(":id_eventoPeaje", $id_evento);
         $sentencia->bindValue(":id_eventoMulta", null);


       }

      $sentencia->bindParam(":importe",$importe);
      $sentencia->bindParam(":id_cuentaCte",$id_cuentaCte);

   $sentencia->execute();

      $this->daoCuentaCte->actualizar($id_cuentaCte,$importe); //actualizo la cuenta Cte con el importe y la fecha

    }

  public function buscarMultasPorCuenta($id_cuentaCte) {

      $sql = "SELECT * FROM " . $this->tabla . " WHERE id_cuentaCte = :id_cuentaCte";
      $obj_pdo = Conexion::getInstance();
      $conexion = $obj_pdo -> conectar();
      $sentencia= $conexion->prepare($sql);
      $sentencia->bindParam(":id_cuentaCte",$id_c);
      $sentencia->execute();


        while ($row = $sentencia->fetch()){

          if($row['id_eventoMulta']){

            $cuentaCte=$this->daoCuentaCte->buscarPorId($id_cuentaCte);
            $movimientoCtaCte = new movimientoCtaCte($cuentaCte);
            $movimientoCtaCte->setFecha($row['fecha_hora']);
            $eventoMulta = $this->daoEventoMulta->buscarPorId($row['id_eventoMulta']);
            $movimiento->setEventoMulta($eventoMulta);

            $movimientos[] = $movimiento;
          }

      }
      if(!empty($movimientos)) return $movimientos;

    }

  public function traerTodos(){
            $sql = "SELECT * FROM " . $this->tabla;
            $obj_pdo = Conexion::getInstance();
            $conexion = $obj_pdo -> conectar();
          $sentencia= $conexion->prepare($sql);
            $sentencia->execute();


              while ($row = $sentencia->fetch()){

                  $cuentaCte=$this->daoCuentaCte->buscarPorId($row['id_cuentaCte']);
                  $movimientoCtaCte = new movimientoCtaCte($cuentaCte);
                  $movimientoCtaCte->setFechaHora($row['fecha_hora']);
                  $movimientoCtaCte->setImporte($row['importe']);
                  if($row['id_eventoMulta']){
                  $eventoMulta = $this->daoEventoMulta->buscarPorId($row['id_eventoMulta']);
                  $movimientoCtaCte->setEventoMulta($eventoMulta);
                  $movimientoCtaCte->setEventoPeaje(null);

                }
                else if($row['id_eventoPeaje']){
                  $eventoPeaje = $this->daoEventoPeaje->buscarPorId($row['id_eventoPeaje']);
                  $movimientoCtaCte->setEventoPeaje($eventoPeaje);
                  $movimientoCtaCte->setEventoMulta(null);
                }


                 $movimientos[] = $movimientoCtaCte;
            }
            if(!empty($movimientos)) return $movimientos;



          }



}

?>
