<?php namespace daos;

require('IDao.php');
/**
 * Esta clase deberia ser SINGLETON sino no funcionaria correctamente
 * de la manera en que está programada. No crear más de una instancia.
 *
 * ¿Por qué?
 */
class TitularJsonDao/* extends SingletonDao implements IDao*/
{
	protected $ruta;
	protected $listado;

	/**
	 * Constructor
	 *
	 * @param string $ruta Ruta del archivo "base de datos"
	 */
	public function __construct($ruta = '/../titulares.txt')
	{
		$ruta = __DIR__. '/'. $ruta;
		if (! file_exists($ruta))
			throw new \Exception('No existe el archivo');

		$this->ruta = $ruta;
		$this->leer();
	}

	/**
	 * Destructor
	 */
	public function __destruct()
	{
		$archivo = fopen($this->ruta, 'w');
		fwrite($archivo, json_encode($this->listado));
		fclose($archivo);
	}

	/**
	 * Inicializa la lista de personas
	 */
	protected function leer()
	{
		$archivo = fopen($this->ruta, "r");
		$lista_titulares = json_decode(fgets($archivo), true);
		$this->mapear($lista_titulares);
		fclose($archivo);
	}

	/**
	 * Transforma el listado de personas en
	 * objetos de la clase Persona
	 *
	 * @param  Array $gente Listado de personas a transformar
	 */
	protected function mapear($lista_titulares)
	{
		$lista_titulares = is_array($lista_titulares) ? $lista_titulares : [];
		$this->listado = array_map(function($t){
			return new \modelos\Titular($t['apellido'], $t['nombre'], $t['dni'], $t['telefono'],$t['m_Usuario']);
		}, $lista_titulares);
	}

	/**
	 * Agrega una persona
	 *
	 * @param Persona $persona Persona a agregar
	 */
	public function agregar($titular)
	{
		$this->listado[] = $titular;
	}

	/**
	 * Devuelve un listado de personas con el
	 * nombre especificado
	 *
	 * @param  String $name Nombre a buscar
	 * @return Array       Listado de personas
	 */
	public function traerPorDni($dni)
	{
		return array_filter($this->listado, function($t) use ($dni) {
			return $t->getDni() == $dni;
		});
	}

	/**
	 * Devuelve un listado con todas las personas
	 *
	 * @return Array Listado de personas
	 */
	public function traerTodos()
	{
		return $this->listado;
	}

}
