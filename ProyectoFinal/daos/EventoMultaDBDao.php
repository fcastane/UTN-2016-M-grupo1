<?php namespace daos;

use daos\Conexion as Conexion;
use daos\SensorSemaforoDBDao as SensorSemaforoDao;
use modelos\EventoMulta as EventoMulta;
use modelos\Sensor_Semaforo as SensorSemaforo;


class EventoMultaDBDao extends Conexion /*implements InterfaceDao*/{

    protected $tabla = "EventosMulta";
    private static $instancia;
    private $daoSemaforo;

    function __construct(){
      $this->daoSemaforo = SensorSemaforoDao::getInstance();
    }

    public static function getInstance(){
        if (  !self::$instancia instanceof self)  {
        self::$instancia = new self;
        }
        return self::$instancia;
      }

    public function traerTodos() { //trae todos los eventos de tipo multa y los retorna  en un array

        $sql = "SELECT * FROM " . $this->tabla;
        $obj_pdo = Conexion::getInstance();

        $conexion = $obj_pdo->conectar();
        $sentencia = $conexion->prepare($sql);

        $sentencia->execute();

        while ($row = $sentencia->fetch()) {
          $sensorSemaforo = $this->daoSemaforo->buscarPorId($row['id_sensorSemaforo']); //busca en DaoSensoresSemaforo el que tenga ese id

          $eventoMulta = new EventoMulta($sensorSemaforo);
          $eventoMulta->setId($row['id_evento']); //Setea al atributo id, el id de la bd
          $eventoMulta->setFecha($row['fecha_hora']); //Setea la fecha hora del evento

				$eventosMulta[] = $eventoMulta;

			}


      //Si el arreglo no esta vacio lo devuelve
      			if(!empty($eventosMulta)) return $eventosMulta;

    }

    public function agregar(EventoMulta $eventoMulta) { //agrega y devuelve el id

        $sql = "INSERT INTO " . $this->tabla . " (id_sensorSemaforo,fecha_hora) VALUES (:id_sensorSemaforo,NOW())";

        $obj_pdo = Conexion::getInstance();
        $conexion = $obj_pdo->conectar();
       $sentencia = $conexion->prepare($sql);


        $idSensorSemaforo=$this->daoSemaforo->obtenerId($eventoMulta->getSensor()); //trae el id del Sensor Semaforo de la tabla sensorSemaforo
        $sentencia->bindParam(":id_sensorSemaforo", $idSensorSemaforo);


        // Ejecuto la sentencia.
        $sentencia->execute();

         $lastId = $conexion->lastInsertId();
         return $lastId;

    }

    public function buscarPorId($id_eventoMulta) {

        $sql = "SELECT * FROM " . $this->tabla . " WHERE id_eventoMulta = :id_eventoMulta";
        $obj_pdo = Conexion::getInstance();
        $conexion = $obj_pdo -> conectar();
        $sentencia= $conexion->prepare($sql);

        $sentencia->bindParam(":id_eventoMulta",$id_eventoMulta);
        $sentencia->execute();

        $row=$sentencia->fetch();
          if ($row){

            $sensorSemaforo = $this->daoSemaforo->buscarPorId($row['id_sensorSemaforo']); //busca en DaoSensoresSemaforo el que tenga ese id

            $eventoMulta = new EventoMulta($sensorSemaforo);
            $eventoMulta->setId($row['id_eventoMulta']); //Setea al atributo id, el id de la bd
            $eventoMulta->setFecha($row['fecha_hora']); //Setea la fecha  y hora del evento
            return $eventoMulta;
        }


      }


}

?>
