<?php namespace daos;

//require_once ('IVehiculosDao.php');

class VehiculoJsonDao /*extends SingletonDao*/ /*implements IDao*/{

  protected $ruta;
  protected $listado;

public function __construct($ruta = '/../vehiculos.txt'){
//public function __construct($ruta = __DIR__ .'/../vehiculos.txt'){
    // ESTO LO HZO martin provisorio
   $ruta = __DIR__. '/'. $ruta;
    if (! file_exists($ruta))
			throw new Exception('No existe el archivo');

		$this->ruta = $ruta;
		$this->leer();
  }

  public function __destruct()
	{
		$archivo = fopen($this->ruta, 'w');
		fwrite($archivo, json_encode($this->listado));
		fclose($archivo);
	}

  protected function leer()
  {
    $archivo = fopen($this->ruta, "r");
    $lista_vehiculos = json_decode(fgets($archivo), true);
    $this->mapear($lista_vehiculos);
    fclose($archivo);
  }

  protected function mapear($lista_vehiculos)
	{
		$lista_vehiculos = is_array($lista_vehiculos) ? $lista_vehiculos : [];
		$this->listado = array_map(function($v){
			return new \modelos\Vehiculo($v['titular'], $v['dominio'], $v['marca'],$v['modelo']);
		}, $lista_vehiculos);
	}

  public function agregar($Vehiculo)
  {
    $this->listado[] = $Vehiculo;
  }

  public function listar()
	{
		return $this->listado;
	}


}
