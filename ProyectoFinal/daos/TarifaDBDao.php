<?php namespace daos;

use daos\Conexion as Conexion;
use modelos\Tarifa as Tarifa;

class TarifaDBDao extends Conexion /*implements InterfaceDao*/{

    protected $tabla = "Tarifas";
    private static $instancia;


    public static function getInstance()
    {
    if (  !self::$instancia instanceof self)
    {
       self::$instancia = new self;
    }
    return self::$instancia;
  }
    public function traerUno($id_tarifa) {
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id_tarifa= :id_tarifa ";
      $obj_pdo = Conexion::getInstance();
      $conexion = $obj_pdo -> conectar();
      $sentencia= $conexion->prepare($sql);

      $sentencia->bindParam(":id_tarifa",$id_tarifa);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {
        $tarifa= new Tarifa($row['fecha_desde'],$row['fecha_hasta'],$row['multa'],$row['peaje_hora_pico'],$row['peaje_hora_real']);
        return $tarifa;

      }

    }



  }
