<?php namespace daos;

use daos\Conexion as Conexion;
use modelos\Usuario as Usuario;
use modelos\Rol as Rol;
use daos\RolDBDao as RolDao;

class UsuarioDBDao extends Conexion /*implements InterfaceDao*/{

    protected $tabla = "Usuarios";
    private static $instancia;
    private $daoRol;

    function __construct(){
      $this->daoRol = RolDao::getInstance();
    }
    public static function getInstance()
    {
    if (  !self::$instancia instanceof self)
    {
       self::$instancia = new self;
    }
    return self::$instancia;
  }

    public function agregar($usuario) {

        // Guardo como string la consulta sql utilizando como values, marcadores de parámetros con nombre (:name) o signos de interrogación (?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada
        $sql = "INSERT INTO " . $this->tabla . " (nombre,password,id_rol) VALUES (:nombre,:password,:id_rol)";

        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();

        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();

        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);

        $nombre=$usuario->getNombre();
        $password=$usuario->getPassword();
        $id_rol = 1; //1 Es el id para usuario tipo titular
        //FALTARIA HACER UNA FUNCION QUE TRAIGA EL ID DEL PARA ESE TIPO DE USUARIO

        // Reemplazo los marcadores de parametro por los valores reales utilizando el método bindParam().
        $sentencia->bindParam(":nombre", $nombre);
        $sentencia->bindParam(":password", $password);
        $sentencia->bindParam(":id_rol", $id_rol);

        // Ejecuto la sentencia.
        $sentencia->execute();
        $lastId = $conexion->lastInsertId();
         return $lastId;
    }

    public function traerUno($id_usuario) {
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id_usuario = :id_usuario";
      $obj_pdo = Conexion::getInstance();
      $conexion = $obj_pdo -> conectar();
   $sentencia= $conexion->prepare($sql);
      $sentencia->bindValue(":id_usuario", $id_usuario);

      $sentencia->execute();
      $row = $sentencia->fetch();
      if($row) {
    $rol= $this->daoRol->traerUno($row['id_rol']);

        $usuario = new Usuario($row['nombre'],$row['password'],$rol);
        return $usuario;
      }

    }

    public function traerTodos() {

        // Guardo como string la consulta sql
        $sql = "SELECT * FROM " . $this->tabla;


        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();


        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();


        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);


        // Ejecuto la sentencia.
        $sentencia->execute();

        while ($row = $sentencia->fetch()) {
          $rol = $this->daoRol->traerUno($row['id_rol']);
          $usuario = new Usuario($row['nombre'],$row['password'],$rol);
				$usuarios[] = $usuario;

			}


      //Si el arreglo no esta vacio lo devuelve
      			if(!empty($usuarios)) return $usuarios;
    }

  





}
