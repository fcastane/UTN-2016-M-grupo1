<?php namespace daos;

use daos\Conexion as Conexion;
use daos\SensorPeajeDBDao as SensorPeajeDao;
use modelos\EventoPeaje as EventoPeaje;
use modelos\Sensor_Peaje as SensorPeaje;


class EventoPeajeDBDao extends Conexion /*implements InterfaceDao*/{

    protected $tabla = "EventosPeaje";
    private static $instancia;
    private $daoPeaje;

    function __construct(){
      $this->daoPeaje = SensorPeajeDao::getInstance();
    }

    public static function getInstance(){
        if (  !self::$instancia instanceof self)  {
        self::$instancia = new self;
        }
        return self::$instancia;
      }



    public function traerTodos() {

        // Guardo como string la consulta sql
        $sql = "SELECT * FROM " . $this->tabla;


        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();

        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();

        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
        $sentencia = $conexion->prepare($sql);

        // Ejecuto la sentencia.
        $sentencia->execute();

        while ($row = $sentencia->fetch()) {
            $sensorPeaje = $this->daoPeaje->buscarPorId($row['id_sensorPeaje']);
          //Para cada row que trae, crea un titular seteandole los datos de cada columna
          //lo guarda en un arreglo de objetos titular
          $eventoPeaje = new EventoPeaje($sensorPeaje);
          $eventoPeaje->setId($row['id_eventoPeaje']);
          $eventoPeaje->setFecha($row['fecha_hora']);

				$eventosPeaje[] = $eventoPeaje;

			}


      //Si el arreglo no esta vacio lo devuelve
      			if(!empty($eventosPeaje)) return $eventosPeaje;

    }



    public function agregar(EventoPeaje $eventoPeaje) {

        // Guardo como string la consulta sql utilizando como values, marcadores de parámetros con nombre (:name) o signos de interrogación (?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada
        $sql = "INSERT INTO " . $this->tabla . " (id_sensorPeaje,fecha_hora) VALUES (:id_sensorPeaje,NOW())";

        // creo el objeto conexion
        $obj_pdo = Conexion::getInstance();
        // Conecto a la base de datos.
        $conexion = $obj_pdo->conectar();
        // Creo una sentencia llamando a prepare. Esto devuelve un objeto statement
       $sentencia = $conexion->prepare($sql);


        $idSensorPeaje=$this->daoPeaje->obtenerId($eventoPeaje->getSensor());
        // Reemplazo los marcadores de parametro por los valores reales utilizando el método bindParam().

        $sentencia->bindParam(":id_sensorPeaje", $idSensorPeaje);


        // Ejecuto la sentencia.
        $sentencia->execute();

         $lastId = $conexion->lastInsertId();
         return $lastId;

    }

    public function buscarPorId($id_eventoPeaje) {

        $sql = "SELECT * FROM " . $this->tabla . " WHERE id_eventoPeaje = :id_eventoPeaje";
        $obj_pdo = Conexion::getInstance();
        $conexion = $obj_pdo -> conectar();
        $sentencia= $conexion->prepare($sql);

        $sentencia->bindParam(":id_eventoPeaje",$id_eventoPeaje);
        $sentencia->execute();


        $row=$sentencia->fetch();
          if ($row){

            $sensorSemaforo = $this->daoPeaje->buscarPorId($row['id_sensorPeaje']); //busca en DaoSensoresSemaforo el que tenga ese id

            $eventoPeaje = new EventoPeaje($sensorSemaforo);
            $eventoPeaje->setId($row['id_eventoPeaje']); //Setea al atributo id, el id de la bd
            $eventoPeaje->setFecha($row['fecha_hora']); //Setea la fecha  y hora del evento
            return $eventoPeaje;


        }
    }


}

?>
