<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Agregar vehiculos</title>
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script type="text/javascript" src="jquery-1.3.2.js"></script>

</head>
<body>
	<div class="container">
		<h1 class="text-center titulo">Ingreso a consulta</h1>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<form method="POST" action="listarMovimientos" >
          <div class="form-group col-md-8">
            <label for="dniTitular">Ingrese su dni:</label>
            <input type="text" name="dniTitular" class="form-control" id="dniTitular" placeholder="Ingrese DNI" autocomplete="off">
            </div>

      <div class="form-group col-md-8">
					<button type="submit" class="btn btn-info">Consultar</button>
					<a class="btn btn-info" href="/">Volver</a>
        </div>
				</form>



			</div>
		</div>
	</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
</body>
</html>
