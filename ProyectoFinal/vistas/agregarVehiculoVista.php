<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Agregar vehiculos</title>
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script type="text/javascript" src="jquery-1.3.2.js"></script>

</head>
<body>
	<div class="container">
		<h1 class="text-center titulo">Agregar Vehiculo</h1>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<form method="POST" action="agregarVehiculo" ><!-- ???????????? como pongo el metodo agregarVehiculo de esa clase <!-- form action="rutaaaaaaaaa URL"-->
					<div class="row">
						<div class="form-group col-md-8">
							<label for="dniTitular">DNI del Titular:</label>
							<input type="text" name="dniTitular" class="form-control" id="dniTitular" placeholder="Ingrese DNI" autocomplete="off">
				  		</div>
						<div class="col-md-4">
							<a href="/adminTitular/altaTitular" class="btn btn-primary" role="button" aria-disabled="true">Nuevo Titular</a>

						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-4">
							<label for="dominio">Dominio:</label>
							<input type="text" name="dominio" class="form-control" id="dominio" placeholder="AAA000" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label for="marca">Marca</label>
						<select class="form-control" name="marca" id="marca">
				            <option>Fiat</option>
				            <option>Peugeot</option>
				            <option>Ford</option>
				            <option>Chevrolet</option>
				            <option >Volwagsneghesg</option>
				            <option >Toyota</option>
       					</select>
					</div>
					<div class="form-group">
						<label for="modelo">Modelo</label>
						<select class="form-control" name="modelo" id="modelo">
							<option>Linea</option>
						</select>

					</div>
					<button type="submit" class="btn btn-info">Registrar</button>
					<a class="btn btn-info" href="/">Volver</a>
				</form>

			</div>
		</div>
	</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
</body>
</html>
