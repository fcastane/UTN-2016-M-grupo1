<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Trafi-Mdq</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">


  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">
            <img alt="Logo" style="height: 55px; margin-top: -15px;" src="/img/logo1.png">
          </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
           <li><a href="/">Inicio<span class="sr-only">(current)</span></a></li>
           <li><a href="/adminVehiculo/altaVehiculo">Agregar Vehiculos</a></li>
           <li><a href="/adminVehiculo/index">Listar vehiculos</a></li>
           <li><a href="/adminTitular/index">Listar Titulares</a></li>
           <li><a href="/simulacion/index">Simulación</a></li>
           <li><a href="/consulta/index">Consultar mi saldo</a></li>




       </div>
      </div>
    </nav>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </body>
</html>
<script>
  var url = window.location; // Will only work if string in href matches with location
   $('ul.nav a[href="'+ url +'"]').parent().addClass('active'); // Will also work for relative and absolute hrefs
  $('ul.nav a').filter(function() { return this.href == url; }).parent().addClass('active');
</script>
