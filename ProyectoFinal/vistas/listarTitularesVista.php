<!DOCTYPE html>
<html>
<head>
	<title>Listado de titulares</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css" media="screen" title="no title">


</head>
<body>
  <h1 class="text-center titulo">Listado de titulares</h1>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <table class="table">
          <thead>
            <tr>
              <th>Apellido</th>
              <th>Nombre</th>
              <th>DNI</th>
              <th>Telefono</th>
            </tr>
          </thead>
          <tbody>
          <?php
          foreach($listado as $titular)
          { ?>
          <tr>
            <td><?php echo $titular->getApellido(); ?></td>
            <td><?php echo $titular->getNombre(); ?></td>
            <td><?php echo $titular->getDni(); ?></td>
            <td><?php echo $titular->getTelefono(); ?></td>
          </tr>
          <?php }
          ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-right">
        <a class="btn btn-info" href="/adminVehiculo/altaVehiculo">Volver a registración vehiculo</a>
      </div>
    </div>
  </div>
 </body>
 </html>
