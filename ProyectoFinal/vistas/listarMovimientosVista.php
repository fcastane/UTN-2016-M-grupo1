<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Consulta de movimientos</title>
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script type="text/javascript" src="jquery-1.3.2.js"></script>

</head>
<body>
	<div class="container">
		<h1 class="text-center titulo">Consulta de movimientos</h1>
		<div class="row">
			<div class="col-md-12 ">
				<form method="POST" action="cargarMovimiento" >
					<div class="row">
					<div class="form-group col-md-8">
						<label for="titular">Titular</label>
						<table class="table">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Apellido</th>
									<th>DNI</th>
									<th>Telefono</th>
									<th>Usuario</th>
									<th>Contraseña</th>

								</tr>
							</thead>
						<tbody>
				<tr>
					<td><?php echo $titular->getNombre(); ?></td>
					<td><?php echo $titular->getApellido(); ?></td>
					<td><?php echo $titular->getDni(); ?></td>
					<td><?php echo $titular->getTelefono(); ?></td>
					<td><?php echo $titular->getUsuario()->getNombre(); ?></td>
					<td><?php echo $titular->getUsuario()->getPassword(); ?></td>

				</tr>
				</tbody>
				</table>
	</div>
</div>

							<?php foreach($listadoVehiculos as $vehiculo)	{ ?>
								<div class="row">
								<div class="form-group col-md-4">
									<label for="dominio">Vehiculo</label>
									<table class="table">
										<thead>
											<tr>
												<th>Dominio</th>
												<th>Marca</th>
												<th>Modelo</th>
												<th>QR</th
											</tr>
										</thead>
									<tbody>
							<tr>
								<td><?php echo $vehiculo->getDominio(); ?></td>
								<td><?php echo $vehiculo->getMarca(); ?></td>
								<td><?php echo $vehiculo->getModelo(); ?></td>
								<td><img src="<?php echo $vehiculo->getQr();?>"></td>
							</tr>
						</tbody>
					</table>
				</div>

							<?php	foreach($listadoCuentasCte as $cuentaCte)	{
								 if($cuentaCte->getNumero()==$vehiculo->getCuentaCte()->getNumero()){ ?>
											<div class="form-group col-md-6">
													<label for="dominio">Cuenta Corriente</label>
													<table class="table">
														<thead>
															<tr>
																<th>Nº</th>
																<th>Fecha ultima actualización</th>
																<th>Saldo actual</th>
															</tr>
														</thead>
														<tbody>
										<tr>
											<td><?php echo $cuentaCte->getNumero(); ?></td>
											<td><?php echo $cuentaCte->getFecha(); ?></td>
											<td><?php echo $cuentaCte->getSaldo(); ?></td>
										</tr>

										<?php
										?>
									</tbody>
								</table>
							</div>
							<div class="form-group col-md-6">
									<label>Eventos Multa</label>
									<table class="table">
										<thead>
											<tr>
												<th>Fecha:Hora</th>
												<th>Importe</th>
												<th>Nº Sensor</th>

											</tr>
										</thead>
							<?php

										foreach($listadoMovimientos as $movimiento)
										{
												if($movimiento->getCuentaCte()->getNumero()==$cuentaCte->getNumero()){
													if(!is_null($movimiento->getEventoMulta())){
											?>
											<tbody>

											<td><?php echo $movimiento->getFecha() ?></td>
											<td><?php echo $movimiento->getImporte(); ?></td>
											<td><?php echo $movimiento->getEventoMulta()->getSensor()->getNumeroSerie(); ?></td>
										</tbody>


										<?php }}
									}	?>
								</table>


									<label>Eventos Peaje</label>
									<table class="table">
										<thead>
											<tr>
												<th>Fecha:Hora</th>
												<th>Importe</th>
												<th>Nº Sensor</th>

											</tr>
										</thead>
							<?php

										foreach($listadoMovimientos as $movimiento)
										{
												if($movimiento->getCuentaCte()->getNumero()==$cuentaCte->getNumero()){

													if(!is_null($movimiento->getEventoPeaje())){
											?>
											<tbody>

											<td><?php echo $movimiento->getFecha() ?></td>
											<td><?php echo $movimiento->getImporte(); ?></td>
											<td><?php echo $movimiento->getEventoPeaje()->getSensor()->getNumeroSerie();?></td>
										</tbody>


										<?php }}}	?>
								</table>

							</div>


						</div>

							<?php  }}}
							?>




				</form>



			</div>
		</div>
	</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
</body>
</html>
