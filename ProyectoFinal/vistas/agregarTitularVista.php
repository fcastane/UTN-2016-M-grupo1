<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Agregar titulares</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <h1 class="text-center titulo">Agregar Titular</h1>
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
    <form method="POST" action="agregarTitular">
      <div class="form-group">
        <label for="apellido">Apellido:</label>
        <input type="text" name="apellido" class="form-control" id="apellido" autocomplete="off">
      </div>
      <div class="form-group">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" class="form-control" id="nombre"  autocomplete="off">
      </div>
      <div class="form-group">
        <label for="dni">DNI:</label>
        <input type="text" name="dni" class="form-control" id="dni" autocomplete="off">
      </div>
      <div class="form-group">
        <label for="dni">Telefono:</label>
        <input type="text" name="telefono" class="form-control" id="telefono" autocomplete="off">
      </div>
      <button type="submit" class="btn btn-info">Registrar</button>
      <a class="btn btn-info" href="/">Volver</a>
    </form>
  </div>
  </div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
