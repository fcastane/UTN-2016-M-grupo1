<!DOCTYPE html>
<html>
<head>
	<title>Listado de Vehiculos</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css" media="screen" title="no title">


</head>
<body>
  <h1 class="text-center titulo">Listado de Vehiculos</h1>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <table class="table">
          <thead>
            <tr>
							<th>Titular</th>
              <th>Dominio</th>
              <th>Marca</th>
              <th>Modelo</th>
							<th>QR</th>

            </tr>
          </thead>
          <tbody>
          <?php
          foreach($listado as $vehiculo)
          { ?>
          <tr>
						<td><?php echo $vehiculo->getTitular()->getDni(); ?></td>
            <td><?php echo $vehiculo->getDominio(); ?></td>
            <td><?php echo $vehiculo->getMarca(); ?></td>
            <td><?php echo $vehiculo->getModelo(); ?></td>
						<td><img src="<?php echo $vehiculo->getQr();?>"></td>

          </tr>
          <?php }
          ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-right">
        <a class="btn btn-info" href="/adminVehiculo/altaVehiculo">Volver a registración vehiculo</a>
      </div>
    </div>
  </div>
 </body>
 </html>
