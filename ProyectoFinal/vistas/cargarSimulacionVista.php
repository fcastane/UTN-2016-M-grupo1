<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Agregar vehiculos</title>
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script type="text/javascript" src="jquery-1.3.2.js"></script>

</head>
<body>
	<div class="container">
		<h1 class="text-center titulo">Simulación Flujo Vehicular</h1>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<form method="POST" action="cargarMovimiento" >
						<div class="form-group col-md-8">
							<label for="dominio">Seleccione vehiculo:</label>
              <select name="dominio">
                <?php
                foreach($listadoVehiculos as $vehiculo)
                { ?>
                  <option ><?php echo $vehiculo->getDominio(); ?></option>
                <?php }
                ?>
              </select>
				  		</div>

						<div  class="form-group col-md-8">
							<label for="tipoSensor">Tipo de Sensor:</label>
							<select id="status" name="tipoSensor" onChange="mostrar(this.value);">
								  <option value="Semaforo">Semaforo</option>
								  <option value="Peaje">Peaje</option>
							</select>

						</div>

						<div id="semaforo" style="display: none;">
							<div class="form-group col-md-8">
	 						 <label for="dominio">Nº Serie Sensor:</label>
	 						 <select name="numeroSerie">
	 							 <?php
	 							 foreach($listadoSemaforos as $sensor)
	 							 { ?>
	 								 <option><?php echo $sensor->getNumeroSerie(); ?></option>
	 							 <?php }
	 							 ?>
	 						 </select>
	 						 </div>

							 	<div class="form-group col-md-8">
					<button type="submit" class="btn btn-info">Simular</button>
					<a class="btn btn-info" href="/">Volver</a>
				</div>
				</div>



					<div id="peaje" style="display: none;">


					 <div class="form-group col-md-8">
						 <label for="dominio">Nº Serie Sensor:</label>
						 <select name="numeroSerie2">
							 <?php
							 foreach($listadoPeajes as $sensor)
							 { ?>
								 <option><?php echo $sensor->getNumeroSerie(); ?></option>
							 <?php }
							 ?>
						 </select>
						 </div>

						 <label for="franja">Seleccion Franja Horaria:</label>
 						<div class="form-group">
 						 	<input type="radio" name="franja" value="Pico" checked> Hora Pico (7 a 10 y 17 a 20) <br>
 						 <input type="radio" name="franja" value="No Pico"> Hora no Pico (resto) <br>
 					 </div>

					<button type="submit" class="btn btn-info">Simular</button>
					<a class="btn btn-info" href="/">Volver</a>

			</div>
		</form>

		</div>
	</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
</body>
</html>

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript">
function mostrar(id) {
    if (id == "Semaforo") {
        $("#semaforo").show();
        $("#peaje").hide();

    }

    if (id == "Peaje") {
        $("#semaforo").hide();
        $("#peaje").show();

    }


}
</script>
