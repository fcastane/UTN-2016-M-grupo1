<?php namespace controladoras;

//use daos\TitularJsonDao as TitularDao;
use modelos\Titular as Titular;
use modelos\Usuario as Usuario;
use modelos\Rol as Rol;
use daos\TitularDBDao as TitularDao;
use daos\UsuarioDBDao as UsuarioDao;
use daos\RolDBDao as RolDao;



  class adminTitularControlador
  {
    private $daoTitular;

    function __construct(){
      $this->daoTitular = TitularDao::getInstance();
    }
    public function altaTitular(){ //Trae la vista de agregar un titular
      include_once ('../vistas/agregarTitularVista.php');
    }

    public function index(){ //Muestra el listado de titulares

      $listado= $this->daoTitular->traerTodos();
         require ('../vistas/listarTitularesVista.php');
    }

    private function generaPassword(){
      //Se define una cadena de caractares.
      $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
      //Obtenemos la longitud de la cadena de caracteres
      $longitudCadena=strlen($cadena);

      //Se define la variable que va a contener la contraseña
      $pass = "";
      //Se define la longitud de la contraseña
      $longitudPass=10;

      //Creamos la contraseña
      for($i=1 ; $i<=$longitudPass ; $i++){
          //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
          $pos=rand(0,$longitudCadena-1);

          //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
          $pass .= substr($cadena,$pos,1);
      }
      return $pass;
    }



    public function agregarTitular(){ //toma los datos ingresados en el formulario y los guarda en el daoTitular

      //Comprueba que todos los campos esten completos
      if (empty($_POST['apellido']) || empty($_POST['nombre']) || empty($_POST['dni']) || empty($_POST['telefono'] ) )
      throw new \Exception('Debe pasar todos los valores');

      $apellido = $_POST['apellido'];
      $nombre= $_POST['nombre'];
      $dni= $_POST['dni'];
      $telefono= $_POST['telefono'];
      if (!$this->daoTitular->buscarPorDni($dni)){ //Busca algun titular con ese dni, si es null...

        $rol = new Rol('Cliente');

        $usuario = new Usuario($dni,$this->generaPassword(),$rol);//Creo una instancia de usuario
        $titular = new Titular($apellido, $nombre,$dni, $telefono, $usuario);//Creo una instancia de titular
        $this->daoTitular->agregar($titular);//Agrego el titular creado en el daotitular
        $listado= $this->daoTitular->traerTodos();
        require ('../vistas/listarTitularesVista.php');
      }

      else echo 'Ya existe un titular con ese dni';

    }





    }



 ?>
