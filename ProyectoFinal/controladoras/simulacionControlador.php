<?php namespace controladoras;

use daos\VehiculoDBDao as VehiculoDao;
use daos\MovimientoCtaCteDBDao as MovimientoDao;
use daos\SensorDBDao as SensorDao;
use daos\SensorPeajeDBDao as SensorPeajeDao;
use daos\SensorSemaforoDBDao as SensorSemaforoDao;


use modelos\EventoMulta as EventoMulta;
use modelos\EventoPeaje as EventoPeaje;
use modelos\Sensor_Semaforo as SensorSemaforo;
use modelos\Sensor_Peaje as SensorPeaje;
use modelos\MovimientoCtaCte as MovimientoCtaCte;


class simulacionControlador{
  private $daoVehiculo;
  private $daoSensor;
  private $daoSensorSemaforo;
  private $daoSensorPeaje;
  private $daoMovimientoCtaCte;



  function __construct(){
    $this->daoVehiculo = VehiculoDao::getInstance();
    $this->daoSensor = SensorDao::getInstance();
    $this->daoSensorSemaforo = SensorSemaforoDao::getInstance();
    $this->daoSensorPeaje = SensorPeajeDao::getInstance();
    $this->daoMovimientoCtaCte = MovimientoDao::getInstance();




  }


  public function index(){ //Carga el formulario de simulacion

      $listadoVehiculos= $this->daoVehiculo->traerTodos();
      $listadoSemaforos = $this->daoSensorSemaforo->traerTodos();
      $listadoPeajes = $this->daoSensorPeaje->traerTodos();
      require ('../vistas/cargarSimulacionVista.php');
  }

  public function cargarMovimiento(){

      $tipoSensor = $_POST['tipoSensor'];
      $dominio= $_POST['dominio'];

      $vehiculo=$this->daoVehiculo->buscarPorDominio($dominio); //Traigo el vehiculo con ese dominio
      $cuentaCte=$vehiculo->getCuentaCte(); //Devuelvo la cuentaCte de ese vehiculo


      $movimientoCtaCte = new MovimientoCtaCte($cuentaCte);

      if($tipoSensor=='Semaforo'){ //Si el tipo de sensor seleccionado fue semaforo
        $numeroSerie= $_POST['numeroSerie'];
        $sensor=$this->daoSensor->buscarPorNumeroSerie($numeroSerie); //busca el Sensor con el numero de serie seleccionado en el formulario

        $sensorSemaforo = new SensorSemaforo($sensor->getFechaAlta(), $sensor->getLatitud(), $sensor->getLongitud(), $sensor->getNumeroSerie());
        $eventoMulta = new EventoMulta($sensorSemaforo);
        $movimientoCtaCte->setEventoMulta($eventoMulta); //Setea el evento Multa en el movimiento de la Cta Cte, los demas eventos seran null
      }
      else{ //Si no, es decir el seleccionado fue Peaje
        $numeroSerie= $_POST['numeroSerie2'];
        $sensor=$this->daoSensor->buscarPorNumeroSerie($numeroSerie); //busca el Sensor con el numero de serie seleccionado en el formulario
        $franjaHoraria = $_POST['franja']; //traemos la franja seleccionada
        $sensorPeaje = new SensorPeaje($sensor->getFechaAlta(), $sensor->getLatitud(), $sensor->getLongitud(), $sensor->getNumeroSerie());
        $eventoPeaje = new EventoPeaje($franjaHoraria,$sensorPeaje);
        $movimientoCtaCte->setEventoPeaje($eventoPeaje);

      }

      $this->daoMovimientoCtaCte->agregar($movimientoCtaCte); //agrega el movimiento a la bd

  }

  }


 ?>
