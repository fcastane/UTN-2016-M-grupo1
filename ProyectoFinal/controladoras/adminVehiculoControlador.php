<?php namespace controladoras;

//  use daos\VehiculoJsonDao as VehiculoDao;
  use daos\VehiculoDBDao as VehiculoDao;
  use daos\TitularDBDao as TitularDao;

  use modelos\Vehiculo as Vehiculo;
  use modelos\CuentaCte as CuentaCte;
  use modelos\QrGenerator as Qr;

  class adminVehiculoControlador
  {
        private $daoVehiculo;
        private $daoTitular;

        function __construct(){
          $this->daoVehiculo = VehiculoDao::getInstance();
          $this->daoTitular = TitularDao::getInstance();

        }

    public function index(){ //Trae el listado de vehiculos
      $listado = $this->daoVehiculo->traerTodos();
      require('../vistas/listarVehiculosVista.php');
    }

    public function altaVehiculo(){
      require ('../vistas/agregarVehiculoVista.php');
    }

    public function agregarVehiculo(){ //toma los datos ingresados en el formulario y los guarda en el daoVehiculo
      //Comprueba que todos los campos esten completos
        if (empty($_POST['dniTitular']) || empty($_POST['dominio']) || empty($_POST['marca']) || empty($_POST['modelo'] ) )
        throw new \Exception('Debe pasar todos los valores');

      $dominio= $_POST['dominio'];
      $modelo= $_POST['modelo'];
      $marca= $_POST['marca'];
      $dniTitular = $_POST['dniTitular'];

    if($this->validarFormatoDominio($dominio)){ //Valida el formato de la patente, si esta ok...

      $titular=$this->daoTitular->buscarPorDni($dniTitular); //Busca el dni titular en la BD
      if($titular){ //Si existe...

         $vehiculo =$this->daoVehiculo->buscarPorDominio($dominio); //Busca algun vehiculo con ese dominio
          if(!$vehiculo) { //Si es null, es decir no existe un vehiculo con ese dominio...

            $qr= "https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl=".$dominio."&choe=UTF-8";
            $cuentaCte = new CuentaCte();

            $vehiculo = new Vehiculo($dominio,$marca,$modelo,$qr,$titular,$cuentaCte);

            $this->daoVehiculo->agregar($vehiculo); //Agrega el vehiculo a la bd
            $listado = $this->daoVehiculo->traerTodos(); //Los trae y los lista

            require ('../vistas/listarVehiculosVista.php');
          }
          else{
          echo "Ya existe un vehiculo con ese dominio.";
        }


    }
    else{
      echo "No existe un titular con ese dni. Debe crearlo.";
    }
  }
  else{
      echo "Escriba un dominio con un formato valido";
  }

    }
    //falta que chequee si el titular existe y si existe lo devuelva y si no muestre un mensaje de: "Titular no se encuentra, debe Registrar uno nuevo"
    public function validarFormatoDominio($dominio){

     $expr='/^[a-z]{3}[0-9]{3}\b/'; //El dominio debe contener 3 letras de la a la z, y tres numeros del 0 al 9
     if(preg_match($expr, strtolower($dominio))){
       return true;
     }


   }
  }
 ?>
