<?php namespace controladoras;

use daos\TitularDBDao as TitularDao;
use daos\VehiculoDBDao as VehiculoDao;
use daos\CuentaCteDBDao as CuentaCteDao;
use daos\MovimientoCtaCteDBDao as MovimientoCtaCteDao;


  class consultaControlador
  {
      private $daoTitular;
      private $daoVehiculo;
      private $daoCuenta;
      private $daoMovimientoCtaCte;

        function __construct(){
          $this->daoTitular  = TitularDao::getInstance();
          $this->daoVehiculo = VehiculoDao::getInstance();
          $this->daoCuenta = CuentaCteDao::getInstance();
          $this->daoMovimientoCtaCte = MovimientoCtaCteDao::getInstance();
          $this->daoEventoMulta= \daos\EventoMultaDBDao::getInstance();//temporal

        }

    public function index(){
   $listaMovimientos=$this->daoMovimientoCtaCte->traerTodos();
      require('../vistas/consultarSaldoVista.php');
    }

    public function listarMovimientos(){

      if (empty($_POST['dniTitular'])   )
      throw new \Exception('Debe completar el dni.');

      $dniTitular = $_POST['dniTitular'];
      $titular=$this->daoTitular->buscarPorDni($dniTitular); //Busca el dni titular en la BD
      if($titular){ //Si existe...
        $id_titular = $this->daoTitular->buscarId($titular); //Trae el id del titular en la BD
        $listadoVehiculos = $this->daoVehiculo->traerPorIdTitular($id_titular);
        $listadoCuentasCte=$this->daoCuenta->traertodos();
        $listadoMovimientos=$this->daoMovimientoCtaCte->traerTodos();
        require('../vistas/listarMovimientosVista.php');
      }
      else echo "No existe un titular con ese dni.";

      }

  }
 ?>
