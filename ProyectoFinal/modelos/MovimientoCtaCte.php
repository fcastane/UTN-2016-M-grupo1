<?php namespace modelos;

/**
 * @author Usuario
 * @version 1.0
 * @created 25-oct-2016 8:44:35
 */
class MovimientoCtaCte{

	private $fecha_hora;
	private $importe;
	public $m_cuentaCte;
	public $m_EventoPeaje;
	public $m_Pago;
	public $m_EventoMulta;

	function __construct(CuentaCte $m_cuentaCte=null){

		$this->m_cuentaCte =$m_cuentaCte;
	}
	function __destruct()
	{
	}
	public function getCuentaCte(){
			return $this->m_cuentaCte;
	}
	public function getEventoPeaje(){
		return $this->m_EventoPeaje;
	}
	public function getEventoMulta(){
		return $this->m_EventoMulta;
	}
	public function getImporte(){
		return $this->importe;
	}

	public function getFecha(){
	return	$this->fecha_hora;
	}

	public function setFechaHora($fecha_hora)
	{
		$this->fecha_hora = $fecha_hora;
	}

	public function setEventoPeaje($eventoPeaje)
	{
		$this->m_EventoPeaje = $eventoPeaje;
	}


	public function setEventoMulta($eventoMulta)
	{
		$this->m_EventoMulta = $eventoMulta;
	}

public function setImporte($importe){
	$this->importe=$importe;
}




}
?>
