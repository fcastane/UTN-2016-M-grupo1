<?php namespace modelos;

/**
 * @author Usuario
 * @version 1.0
 * @created 25-oct-2016 8:44:35
 */
class EventoPeaje{
	private $id;
	private $fecha_hora;
	public $m_Sensor_Peaje;

	function __construct(Sensor_Peaje $m_Sensor_Peaje=null){
		$this->id="";
		$this->fecha_hora = "";
		$this->m_Sensor_Peaje=$m_Sensor_Peaje;
	}

	function __destruct()
	{
	}

	public function getSensor(){
		return $this->m_Sensor_Peaje;
	}

	public function getFecha(){
		return $this->fecha_hora;
	}

	public function getId(){
		return  $this->id;
	}

	public function getFranja(){
		return  $this->franjaHoraria;
	}

	public function setId($id){
		$this->id=$id;
	}

	public function setFecha($fecha){
		$this->fecha_hora=$fecha;
	}


}


?>
