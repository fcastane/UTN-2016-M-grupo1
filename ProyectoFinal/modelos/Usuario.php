<?php namespace modelos;

/**
 * @author Usuario
 * @version 1.0
 * @created 25-oct-2016 8:44:35
 */
class Usuario
{

	protected $nombre;
	protected $password;
	public $m_Rol;


	function __construct($nombre = "", $password= "" , Rol $m_Rol = null){

		$this->nombre = $nombre;
		$this->password = $password;
		$this->m_Rol = $m_Rol;
	}

	function __destruct()
	{
	}

	public function getNombre(){
		return $this->nombre;
	}
	public function getPassword(){
		return $this->password;
	}

	public function getRol(){
		return $this->m_Rol;
	}


}
?>
