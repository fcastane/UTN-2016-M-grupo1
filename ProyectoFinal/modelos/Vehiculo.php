<?php namespace modelos;
class Vehiculo
{
  private $dominio;
	private $marca;
	private $modelo;
	private $qr;
  public $m_cuentaCte;
	public $m_titular;

	function __construct($dominio = "", $marca ="", $modelo = "",$qr="", Titular $m_titular=null, CuentaCte $m_cuentaCte = null){
		$this->dominio = $dominio;
		$this->marca = $marca;
		$this->modelo = $modelo;
		$this->qr = $qr;
		$this->m_titular = $m_titular;
    $this->m_cuentaCte = $m_cuentaCte;

	}


  public function getTitular()
	{
		return $this->m_titular;
	}

  public function getDominio()
  {
    return $this->dominio;
  }

  public function getMarca()
  {
    return $this->marca;
  }

  public function getModelo()
  {
    return $this->modelo;
  }

  public function getQr(){
    return $this->qr;
  }

  public function getCuentaCte()
  {
    return $this->m_cuentaCte;
  }

  public function jsonSerialize() {
      return [
          'titular' => $this->m_titular,
          'dominio' => $this->dominio,
          'marca' => $this->marca,
          'modelo'=> $this->modelo,
          'qr'=> $this->modelo,

      ];
  }
}




 ?>
