<?php namespace modelos;

/**
 * @author Usuario
 * @version 1.0
 * @created 25-oct-2016 8:44:35
 */
class EventoMulta
{
	private $id;
	private $fecha_hora;
	public $m_Sensor_Semaforo;

	function __construct(Sensor_Semaforo $m_Sensor_Semaforo = null){
		$this->id="";
		$this->fecha_hora ="";
		$this->m_Sensor_Semaforo = $m_Sensor_Semaforo;
	}

	function __destruct()
	{
	}

	public function getSensor(){
		return $this->m_Sensor_Semaforo;
	}

	public function getFecha(){
		return $this->fecha_hora;
	}

	public function getId(){
		return  $this->id;
	}

	public function setId($id){
		$this->id=$id;
	}

	public function setFecha($fecha){
		$this->fecha_hora=$fecha;
	}





}
?>
