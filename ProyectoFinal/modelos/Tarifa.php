<?php namespace modelos;




/**
 * @author Usuario
 * @version 1.0
 * @created 25-oct-2016 8:44:35
 */
class Tarifa
{

	private $fecha_desde;
	private $fecha_hasta;
	private $multa;
	private $peaje_hora_pico;
	private $peaje_hora_real;

	function __construct($fecha_desde ="", $fecha_hasta = "", $multa = 0, $peaje_hora_pico = 0, $peaje_hora_real=0){
		$this->fecha_desde = $fecha_desde;
		$this->fecha_hasta = $fecha_hasta;
		$this->multa = $multa;
		$this->peaje_hora_pico = $peaje_hora_pico;
		$this->peaje_hora_real = $peaje_hora_real;

	}

	function __destruct()
	{
	}

	public function getMulta()
	{
		return $this->multa;
	}
	public function getPico(){
		return $this->peaje_hora_pico;

	}

	public function getNoPico(){
		return $this->peaje_hora_real;

	}



}
?>
