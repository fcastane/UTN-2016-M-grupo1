<?php namespace modelos;


/**
 * @author Usuario
 * @version 1.0
 * @created 25-oct-2016 8:44:35
 */
class Sensor {

	protected $fecha_alta;
	protected $latitud;
	protected $longitud;
	protected $numero_serie;

	function __construct($fecha_alta = "", $latitud = "", $longitud = "", $numero_serie = 0){
		$this->fecha_alta = $fecha_alta;
		$this->latitud = $latitud;
		$this->longitud = $longitud;
		$this->numero_serie = $numero_serie;
	}

	function __destruct()
	{
	}

	public function getFechaAlta(){
		return $this->fecha_alta;
	}

	public function getLatitud(){
		return $this->latitud;
	}

	public function getLongitud(){
		return $this->longitud;
	}

	public function getNumeroSerie(){
		return $this->numero_serie;
	}



}
?>
