<?php namespace modelos;



class Titular 
{
	private $apellido;
	private $dni;
	private $nombre;
	private $telefono;
	public $m_Usuario;

	function __construct($apellido = "", $nombre = "",$dni = "", $telefono ="", Usuario $m_Usuario = null){

		$this->apellido = $apellido;
		$this->nombre = $nombre;
		$this->dni = $dni;
		$this->telefono = $telefono;
		$this->m_Usuario = $m_Usuario;

	}

	public function getApellido()
	{
		return $this->apellido;
	}

  public function getNombre()
  {
    return $this->nombre;
  }
	public function getDni()
	{
		return $this->dni;
	}

  public function getTelefono()
  {
    return $this->telefono;
  }
	 /**
	  * @return mixed
	  */
	 public function getUsuario()
	 {
		 return $this->m_Usuario;
	 }

	 public function setApellido($apellido)
	 {
		 $this->apellido=$apellido;
	 }

	 public function setNombre($nombre)
	 {
		 $this->nombre=$nombre;
	 }

	 public function setDni($dni)
	 {
		 $this->dni=$dni;
	 }

	 public function setTelefono($telefono)
	 {
		 $this->telefono=$telefono;
	 }

	 public function setUsuario(Usuario $usuario)
	 {
		 $this->m_Usuario=$usuario;
	 }
	public function jsonSerialize() {
			return [
					'apellido' => $this->apellido,
					'nombre' => $this->nombre,
					'telefono' => $this->telefono,
					'dni'=> $this->dni,
					'm_Usuario'=> $this->m_Usuario,

			];
	}

}
?>
