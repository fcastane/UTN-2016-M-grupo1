<?php namespace modelos;

/**
 * @author Usuario
 * @version 1.0
 * @created 25-oct-2016 8:44:35
 */
class cuentaCte
{
	private $fecha_ultActualizacion;
	private $saldo;
	private $numero;

	function __construct($fecha_ultActualizacion = "", $saldo = 0, $numero=0){
		$this->fecha_ultActualizacion = $fecha_ultActualizacion;
		$this->saldo = $saldo;
		$this->numero= $numero;
	}

	function __destruct(){

	}

	public function getFecha(){
		return $this->fecha_ultActualizacion;
	}

	public function getSaldo(){
		return $this->saldo;
	}

	public function getNumero(){
		return $this->numero;

	}

}
?>
